/*
 * Demo1.cpp
 *
 *  Created on: 7 Jun 2013
 *      Author: allan
 */

// C++ includes
#include <iostream>
#include <iomanip>

// Dinamiko includes
#include <Dinamiko/Dinamiko.hpp>
using namespace Dinamiko;

VectorXd Function(double t, const VectorXd& y)
{
    VectorXd f(3);
    f << -0.04*y[0] + 1.0e+4*y[1]*y[2],
          0.04*y[0] - 1.0e+4*y[1]*y[2] - 3.0e7*y[1]*y[1],
          3.0e7*y[1]*y[1];

    return f;
}

MatrixXd Jacobian(double t, const VectorXd& y)
{
    MatrixXd J = MatrixXd::Zero(3, 3);

    J(0, 0) = -0.04;
    J(0, 1) = 1.0e4*y[2];
    J(0, 2) = 1.0e4*y[1];
    J(1, 0) = 0.04;
    J(1, 1) = -1.0e4*y[2]-6.0e7*y[1];
    J(1, 2) = -1.0e4*y[1];
    J(2, 1) = 6.0e7*y[1];

    return J;
}

int main()
{
    ODEProblem problem;
    problem.SetNumEquations(3);
    problem.SetFunction(Function);
    problem.SetJacobian(Jacobian);

    ODEOptions options;
    options.reltol = 1.0e-6;
    options.abstol = 1.0e-12;
    options.iteration = FUNCTIONAL;
    options.step = BDF;

    ODESolver solver;
    solver.SetOptions(options);
    solver.SetProblem(problem);

    VectorXd y(3);
    y << 1.0, 0.0, 0.0;

    double tstart = 0.0;
    double tfinal = 1.0e10;

    solver.Initialise(tstart, y);

    std::cout << std::setw(10) << std::left << "iter";
    std::cout << std::setw(15) << std::left << "t";
    std::cout << std::setw(15) << std::left << "y0";
    std::cout << std::setw(15) << std::left << "y1";
    std::cout << std::setw(15) << std::left << "y2";
    std::cout << std::endl;

    double t;
    unsigned iter = 1;
    while(t < tfinal)
    {
        solver.Integrate(t, y, tfinal);

        std::cout << std::scientific << std::setprecision(6);
        std::cout << std::setw(10) << std::left << iter++;
        std::cout << std::setw(15) << std::left << t;
        std::cout << std::setw(15) << std::left << y[0];
        std::cout << std::setw(15) << std::left << y[1];
        std::cout << std::setw(15) << std::left << y[2];
        std::cout << std::endl;
    }
}


