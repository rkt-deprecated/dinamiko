/*
 * ODESolver.hpp
 *
 *  Created on: 2 Jun 2013
 *      Author: allan
 */

#pragma once

// Eigen includes
#include <Eigen/Core>
using namespace Eigen;

// Dinamiko includes
#include <Dinamiko/ODE/ODEProblem.hpp>
#include <Dinamiko/ODE/ODEOptions.hpp>

// Forward declarations
struct _generic_N_Vector; typedef struct _generic_N_Vector* N_Vector;

namespace Dinamiko {

/**
 * Class that defined a wrapper to the @c CVODE solver for ordinary differential equations
 */
class ODESolver
{
public:
    /**
     * Convenient type definition for @ref ODEOptions
     */
    typedef ODEOptions Options;

    /**
     * Convenient type definition for @ref ODEProblem
     */
    typedef ODEProblem Problem;

    /**
     * Constructs a default @ref ODESolver instance
     */
    ODESolver();

    /**
     * Destroys any dynamic allocated memory
     */
    virtual ~ODESolver();

    /**
     * Sets the options for the ODE solver
     */
    void SetOptions(const Options& options);

    /**
     * Sets the ODE problem
     */
    void SetProblem(const Problem& problem);

    /**
     * Initialises the ODE solver
     *
     * This method should be invoked whenever the user intends to make a call to @ref Integrate.
     *
     * @param tstart The start time of the integration.
     * @param y The initial values of the variables
     */
    void Initialise(double tstart, const VectorXd& y);

    /**
     * Integrates the ODE performing a single step
     *
     * @param[in,out] t The current time of the integration as input, the new current time as output
     * @param[in,out] y The current variables as input, the new current variables as output
     */
    void Integrate(double& t, VectorXd& y);

    /**
     * Integrates the ODE performing a single step
     *
     * @param[in,out] t The current time of the integration as input, the new current time as output
     * @param[in,out] y The current variables as input, the new current variables as output
     * @param tfinal The final time that the integration must satisfy
     */
    void Integrate(double& t, VectorXd& y, double tfinal);

    /**
     * Solves the ODE equation from @c tstart to @c tfinal
     *
     * @param tstart The start time for the integration
     * @param tfinal The final time for the integration
     * @param[in,out] y The initial value as input, the final value as output
     */
    void Solve(double tstart, double tfinal, VectorXd& y);

private:
    /// The ODE problem
    Problem problem;

    /// The options for the ODE integration
    Options options;

    /// The CVODE context pointer
    void* cvode_mem;

    /// The CVODE vector y
    N_Vector cvode_y;

    /// The auxiliary vector y
    VectorXd y;

    /// The auxiliary vector f for the function evaluation
    VectorXd f;

    /// The auxiliary matrix J for the Jacobian evaluation
    MatrixXd J;
};

} /* namespace Dinamiko */
