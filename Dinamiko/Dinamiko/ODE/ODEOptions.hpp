/*
 * ODEOptions.hpp
 *
 *  Created on: 7 Jun 2013
 *      Author: allan
 */

#pragma once

// Eigen includes
#include <Eigen/Core>
using namespace Eigen;

namespace Dinamiko {

/**
 * The linear multistep method to be used in @ref ODESolver
 */
enum ODEStep
{
    ADAMS, BDF
};

/**
 * The type of nonlinear solver iteration to be used in @ref ODESolver
 */
enum ODEIteration
{
    FUNCTIONAL, NEWTON
};

/**
 * Struct that defines the options for the @ref ODESolver
 */
struct ODEOptions
{
    /**
     * The linear multistep method used in the integration
     */
    ODEStep step = BDF;

    /**
     * The type of nonlinear solver iteration used in the integration
     */
    ODEIteration iteration = NEWTON;

    /**
     * The initial step size to be used in the integration
     *
     * An estimation is made if its value is zero.
     */
    double initial_step = 0.0;

    /**
     * The lower bound on the magnitude of the step size
     */
    double min_step = 0.0;

    /**
     * The upper bound on the magnitude of the step size
     */
    double max_step = 0.0;

    /**
     * The scalar relative error tolerance
     */
    double reltol = 1.0e-6;

    /**
     * The scalar absolute error tolerance
     */
    double abstol = 1.0e-12;

    /**
     * The vector of absolute error tolerances for each component
     */
    VectorXd abstols;

    /**
     * The maximum number of steps to be taken by the solver in its attempt to reach the next output time
     */
    unsigned max_num_steps = 5000;
};

} /* namespace Dinamiko */
