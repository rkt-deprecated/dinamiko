/*
 * ODEProblem.hpp
 *
 *  Created on: 7 Jun 2013
 *      Author: allan
 */

#pragma once

// C++ includes
#include <functional>

// Eigen includes
#include <Eigen/Core>
using namespace Eigen;

// Dinamiko includes
#include <Dinamiko/Utils/Types.hpp>

namespace Dinamiko {

/**
 * Class that defines an ODE problem to be solved with @ref ODESolver
 */
class ODEProblem
{
public:
    /**
     * Convenient type definition for @ref ODEFunction
     */
    typedef ODEFunction Function;

    /**
     * Convenient type definition for @ref ODEJacobian
     */
    typedef ODEJacobian Jacobian;

    /**
     * Convenient type definition for @ref ODEFunctionJacobian
     */
    typedef ODEFunctionJacobian FunctionJacobian;

    /**
     * Convenient type definition for @ref ODEFunctionJacobianResult
     */
    typedef ODEFunctionJacobianResult FunctionJacobianResult;

    /**
     * Constructs a default @ref ODEProblem instance
     */
    ODEProblem();

    /**
     * Sets the number of ordinary differential equations
     */
    void SetNumEquations(unsigned num_equations);

    /**
     * Sets the right-hand side function of the system of ordinary differential equations
     */
    void SetFunction(const Function& f);

    /**
     * Sets the Jacobian of the right-hand side function of the system of ordinary differential equations
     */
    void SetJacobian(const Jacobian& J);

    /**
     * Sets the right-hand side function of the system of ordinary differential equations and its Jacobian
     */
    void SetFunctionJacobian(const FunctionJacobian& fJ);

    /**
     * Gets the number of ordinary differential equations
     */
    unsigned GetNumEquations() const;

    /**
     * Gets the right-hand side function of the system of ordinary differential equations
     */
    const Function& GetFunction() const;

    /**
     * Gets the Jacobian of the right-hand side function of the system of ordinary differential equations
     */
    const Jacobian& GetJacobian() const;

    /**
     * Gets the right-hand side function of the system of ordinary differential equations and its Jacobian
     */
    const FunctionJacobian& GetFunctionJacobian() const;

    /**
     * Checks if the ordinary differential problem was initialised
     *
     * An @ref ODEProblem instance has been initialised if:
     *  - @ref SetNumEquations has been called; and
     *  - @ref SetFunction and @ref SetJacobian have been called, or @ref SetFunctionJacobian was called to set
     *    both the function and jacobian simultaneously.
     */
    bool WasInitialised() const;

    /**
     * Evaluates the right-hand side function of the system of ordinary differential equations
     *
     * @param t The time variable of the function
     * @param y The y-variables of the function
     */
    VectorXd EvalFunction(double t, const VectorXd& y) const;

    /**
     * Evaluates the Jacobian of the right-hand side function of the system of ordinary differential equations
     *
     * @param t The time variable of the function
     * @param y The y-variables of the function
     */
    MatrixXd EvalJacobian(double t, const VectorXd& y) const;

    /**
     * Evaluates the right-hand side function of the system of ordinary differential equations and its Jacobian
     *
     * @param t The time variable of the function
     * @param y The y-variables of the function
     *
     * @see ODEFunctionJacobianResult
     */
    FunctionJacobianResult EvalFunctionJacobian(double t, const VectorXd& y) const;

private:
    /// The number of ordinary differential equations
    unsigned num_equations = 0;

    /// The right-hand side function of the system of ordinary differential equations
    Function f;

    /// The Jacobian of the right-hand side function of the system of ordinary differential equations
    Jacobian J;

    /// The right-hand side function of the system of ordinary differential equations and its Jacobian
    FunctionJacobian fJ;
};

} /* namespace Dinamiko */
