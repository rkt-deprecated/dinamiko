/*
 * ODESolver.cpp
 *
 *  Created on: 2 Jun 2013
 *      Author: allan
 */

#include "ODESolver.hpp"

// C++ includes
#include <stdexcept>

// Sundials includes
#include <cvode/cvode.h>
#include <cvode/cvode_dense.h>
#include <nvector/nvector_serial.h>

// Dinamiko includes
#include <Dinamiko/Utils/Macros.hpp>

#define VecEntry(v, i)    NV_Ith_S(v, i)
#define MatEntry(A, i, j) DENSE_ELEM(A, i, j)

namespace Dinamiko {

struct ODEData
{
    ODEData(const ODEProblem& problem, VectorXd& y, VectorXd& f, MatrixXd& J)
    : problem(problem), y(y), f(f), J(J), num_equations(problem.GetNumEquations())
    {
        y.resize(num_equations);
        f.resize(num_equations);
        J.resize(num_equations, num_equations);
    }

    const ODEProblem& problem;

    VectorXd& y;

    VectorXd& f;

    MatrixXd& J;

    int num_equations;
};

struct ODEError : public std::exception
{
};

struct ODEErrorInitialisation : public std::exception
{
    virtual const char* what() const throw()
    {
        return "Unable to proceed with the initialisation of the ODESolver instance.";
    }
};

struct ODEErrorIntegration : public std::exception
{
    virtual const char* what() const throw()
    {
        return "Unable to proceed with the integration of the ODE with the ODESolver instance.";
    }
};

inline int CVODEStep(const ODEStep& step);
inline int CVODEIteration(const ODEIteration& iteration);

inline void CheckInitialise(int r);
inline void CheckIntegration(int r);

int CVODEFunction(realtype t, N_Vector y, N_Vector ydot, void* user_data);
int CVODEJacobian(long int N, realtype t, N_Vector y, N_Vector fy, DlsMat J, void* user_data, N_Vector tmp1, N_Vector tmp2, N_Vector tmp3);

ODESolver::ODESolver()
: cvode_mem(0), cvode_y(0)
{}

ODESolver::~ODESolver()
{
    // Free the dynamic memory allocated for cvode context
    if(cvode_mem) CVodeFree(&cvode_mem);

    // Free dynamic memory allocated for y
    if(cvode_y) N_VDestroy_Serial(cvode_y);
}

void ODESolver::SetProblem(const Problem& problem_)
{
    problem = problem_;
}

void ODESolver::SetOptions(const Options& options_)
{
    options = options_;
}

void ODESolver::Initialise(double tstart, const VectorXd& y)
{
    // Check if the ordinary differential problem has been initialised
    Assert(problem.WasInitialised(),
        "The ordinary differential problem has not been setup.");

    // Check if the dimension of 'y' matches the number of equations
    Assert(y.size() == problem.GetNumEquations(),
        "The dimension of the working vector does not match the number of differential equations.");

    // The number of differential equations
    const int num_equations = problem.GetNumEquations();

    // Free any dynamic memory allocated for cvode_mem context
    if(cvode_mem) CVodeFree(&cvode_mem);

    // Free dynamic memory allocated for y
    if(cvode_y) N_VDestroy_Serial(cvode_y);

    // Initialise a new vector y
    cvode_y = N_VNew_Serial(num_equations);
    for(int i = 0; i < num_equations; ++i)
        VecEntry(cvode_y, i) = y[i];

    // Initialise a new cvode context
    cvode_mem = CVodeCreate(CVODEStep(options.step), CVODEIteration(options.iteration));

    // Check if the cvode creation succeeded
    if(cvode_mem == NULL) throw ODEErrorInitialisation();

    // Initialise the cvode context
    CheckInitialise(CVodeInit(cvode_mem, CVODEFunction, tstart, cvode_y));

    // Initialise the vector of absolute tolerances
    N_Vector abstols = N_VNew_Serial(num_equations);

    if(options.abstols.size() == num_equations)
        for(int i = 0; i < num_equations; ++i)
            VecEntry(abstols, i) = options.abstols[i];
    else
        for(int i = 0; i < num_equations; ++i)
            VecEntry(abstols, i) = options.abstol;

    // Set the parameters for the calculation
    CheckInitialise(CVodeSetInitStep(cvode_mem, options.initial_step));
    CheckInitialise(CVodeSetMinStep(cvode_mem, options.min_step));
    CheckInitialise(CVodeSetMaxStep(cvode_mem, options.max_step));
    CheckInitialise(CVodeSetMaxNumSteps(cvode_mem, int(options.max_num_steps)));
    CheckInitialise(CVodeSVtolerances(cvode_mem, options.reltol, abstols));

    // Call CVDense to specify the CVDENSE dense linear solver
    CheckInitialise(CVDense(cvode_mem, num_equations));

    // Set the Jacobian function
    CheckInitialise(CVDlsSetDenseJacFn(cvode_mem, CVODEJacobian));

    // Free dynamic memory allocated for `yc`
    N_VDestroy_Serial(abstols);
}

void ODESolver::Integrate(double& t, VectorXd& y)
{
    // Initialise the ODE data
    ODEData data(problem, y, f, J);

    // Set the user-defined data to cvode_mem
    CheckIntegration(CVodeSetUserData(cvode_mem, &data));

    // Solve the ode problem from `tstart` to `tfinal`
    CheckIntegration(CVode(cvode_mem, 2*t, cvode_y, &t, CV_ONE_STEP));

    // Transfer the result from cvode_y to y
    for(int i = 0; i < data.num_equations; ++i)
        y[i] = VecEntry(cvode_y, i);
}

void ODESolver::Integrate(double& t, VectorXd& y, double tfinal)
{
    // Initialise the ODE data
    ODEData data(problem, y, f, J);

    // Set the user-defined data to cvode_mem
    CheckIntegration(CVodeSetUserData(cvode_mem, &data));

    // Solve the ode problem from `tstart` to `tfinal`
    CheckIntegration(CVode(cvode_mem, tfinal, cvode_y, &t, CV_ONE_STEP));

    // Check if the current time is now greater than the final time
    if(t > tfinal)
    {
        // Interpolate y at t using its old state and the new one
        CheckIntegration(CVodeGetDky(cvode_mem, tfinal, 0, cvode_y));

        // Adjust the current time
        t = tfinal;
    }

    // Transfer the result from cvode_y to y
    for(int i = 0; i < data.num_equations; ++i)
        y[i] = VecEntry(this->cvode_y, i);
}

void ODESolver::Solve(double tstart, double tfinal, VectorXd& y)
{
    // Initialise the cvode context
    Initialise(tstart, y);

    // Auxiliary time variable
    double t;

    // Initialise the ODE data
    ODEData data(problem, y, f, J);

    // Set the user-defined data to cvode_mem
    CheckIntegration(CVodeSetUserData(cvode_mem, &data));

    // Solve the ode problem from `tstart` to `tfinal`
    CheckIntegration(CVode(cvode_mem, tfinal, cvode_y, &t, CV_NORMAL));

    // Transfer the result from cvode_y to y
    for(int i = 0; i < data.num_equations; ++i)
        y[i] = VecEntry(this->cvode_y, i);
}

inline int CVODEStep(const ODEStep& step)
{
    switch(step)
    {
        case ADAMS: return CV_ADAMS;
        default: return CV_BDF;
    }
}

inline int CVODEIteration(const ODEIteration& iteration)
{
    switch(iteration)
    {
        case FUNCTIONAL: return CV_FUNCTIONAL;
        default: return CV_NEWTON;
    }
}

inline void CheckInitialise(int r)
{
    if(r != CV_SUCCESS)
        throw ODEErrorInitialisation();
}

inline void CheckIntegration(int r)
{
    if(r != CV_SUCCESS)
        throw ODEErrorIntegration();
}

int CVODEFunction(realtype t, N_Vector y, N_Vector f, void* user_data)
{
    ODEData& data = *static_cast<ODEData*>(user_data);

    for(int i = 0; i < data.num_equations; ++i)
        data.y[i] = VecEntry(y, i);

    data.f = data.problem.EvalFunction(t, data.y);

    for(int i = 0; i < data.num_equations; ++i)
        VecEntry(f, i) = data.f[i];

    return CV_SUCCESS;
}

int CVODEJacobian(long int N, realtype t, N_Vector y, N_Vector fy, DlsMat J, void* user_data, N_Vector tmp1, N_Vector tmp2, N_Vector tmp3)
{
    ODEData& data = *static_cast<ODEData*>(user_data);

    for(int i = 0; i < data.num_equations; ++i)
        data.y[i] = VecEntry(y, i);

    data.J = data.problem.EvalJacobian(t, data.y);

    for(int i = 0; i < data.num_equations; ++i)
        for(int j = 0; j < data.num_equations; ++j)
            MatEntry(J, i, j) = data.J(i, j);

    return CV_SUCCESS;
}

} /* namespace Dinamiko */
