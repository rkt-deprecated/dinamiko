/*
 * ODEProblem.cpp
 *
 *  Created on: 7 Jun 2013
 *      Author: allan
 */

#include "ODEProblem.hpp"

namespace Dinamiko {

ODEProblem::ODEProblem()
{}

void ODEProblem::SetNumEquations(unsigned num_equations_)
{
    num_equations = num_equations_;
}

void ODEProblem::SetFunction(const Function& f_)
{
    // Set the function member
    f = f_;

    // Update the function-jacobian function member
    fJ = [=](double t, const VectorXd& y) { return FunctionJacobianResult({f(t, y), J(t, y)}); };
}

void ODEProblem::SetJacobian(const Jacobian& J_)
{
    // Set the jacobian member
    J = J_;

    // Update the function-jacobian member
    fJ = [=](double t, const VectorXd& y) { return FunctionJacobianResult({f(t, y), J(t, y)}); };
}

void ODEProblem::SetFunctionJacobian(const FunctionJacobian& fJ_)
{
    // Set the function-jacobian member
    fJ = fJ_;

    // Update the function and jacobian members
    f = [=](double t, const VectorXd& y) { return fJ(t, y).f; };
    J = [=](double t, const VectorXd& y) { return fJ(t, y).J; };
}

unsigned ODEProblem::GetNumEquations() const
{
    return num_equations;
}

const ODEFunction& ODEProblem::GetFunction() const
{
    return f;
}

const ODEJacobian& ODEProblem::GetJacobian() const
{
    return J;
}

const ODEFunctionJacobian& ODEProblem::GetFunctionJacobian() const
{
    return fJ;
}

bool ODEProblem::WasInitialised() const
{
    return num_equations > 0 and f and J and fJ;
}

VectorXd ODEProblem::EvalFunction(double t, const VectorXd& y) const
{
    return f(t, y);
}

MatrixXd ODEProblem::EvalJacobian(double t, const VectorXd& y) const
{
    return J(t, y);
}

ODEFunctionJacobianResult ODEProblem::EvalFunctionJacobian(double t, const VectorXd& y) const
{
    return fJ(t, y);
}

} /* namespace Dinamiko */
