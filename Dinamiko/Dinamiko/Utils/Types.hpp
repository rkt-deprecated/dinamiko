/*
 * Types.hpp
 *
 *  Created on: 20 Jun 2013
 *      Author: allan
 */

#pragma once

// C++ includes
#include <functional>

// Eigen includes
#include <Eigen/Core>
using namespace Eigen;

namespace Dinamiko {

/**
 * Struct that defines the result of the evaluation of a vector-valued function and its Jacobian
 */
struct ODEFunctionJacobianResult
{
    /**
     * The result of the evaluation of a vector-valued function
     */
    VectorXd f;

    /**
     * The result of the evaluation of the Jacobian of a vector-valued function
     */
    MatrixXd J;
};

/**
 * Typedef that defines the function signature of the right-hand side function of a system of ordinary differential equations
 */
typedef std::function<VectorXd(double, const VectorXd&)> ODEFunction;

/**
 * Typedef that defines the function signature of the Jacobian of the right-hand side function of a system of ordinary differential equations
 */
typedef std::function<MatrixXd(double, const VectorXd&)> ODEJacobian;

/**
 * Typedef that defines the function signature of the Jacobian of the right-hand side function of a system of ordinary differential equations
 */
typedef std::function<ODEFunctionJacobianResult(double, const VectorXd&)> ODEFunctionJacobian;

} /* namespace Dinamiko */

